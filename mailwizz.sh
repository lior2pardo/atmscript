#!/bin/bash

echo '

  ___ ________  ___   ___        _                                            _           
 / _ \_   _|  \/  |  / _ \      | |                                          | |          
/ /_\ \| | | .  . | / /_\ \_   _| |_ ___  _ __ ___  ___ _ __   ___  _ __   __| | ___ _ __ 
|  _  || | | |\/| | |  _  | | | | __/ _ \| |__/ _ \/ __| |_ \ / _ \| |_ \ / _| |/ _ \ |__|
| | | || | | |  | | | | | | |_| | || (_) | | |  __/\__ \ |_) | (_) | | | | (_| |  __/ |   
\_| |_/\_/ \_|  |_/ \_| |_/\__,_|\__\___/|_|  \___||___/ |__/ \___/|_| |_|\__|_|\___|_|   
                                                       | |                                
                                                       |_|                
' 

echo  "Installing ATMautoresponder Mailwizz Version, Preparing folders please wait ... "

chmod 777 scriptpmta-2015/ -R
mv scriptpmta-2015 / 
cd /scriptpmta-2015/scripts-2015/

	if [ `/usr/bin/getconf LONG_BIT` != 64 ] 
	then 
		echo "Operating system installed on 32-bit, canceled installation ! "
		echo "Reinstall the operating system with CentOS 5 or 6 in 64 bits ! "
		exit
	fi

	if [ `grep -o CentOS /etc/redhat-release` != CentOS ]
	then 
		echo "CentOS not detected, canceled installation ! "
		echo "Reinstall the operating system with CentOS 6 64 bits ! "
		exit
	fi

Release=`cut -d " " -f 3 /etc/redhat-release | cut -d "." -f1`
	if [ $Release == 6 ]
	then
		echo "CentOS 6X 64Bits detected, initiating installation ... "
		./mailwizzinstaller.sh "$1" "$2" "$3" "$4" "$5" "$6" "$7"
		cd /root/
		echo "... ====== installing email hustler script=========== ... "
		./install.sh "$1" "$2" "$3" "$4" "$5" "$6" "$7"	
		echo "... ====== installing campaign creater=========== ... "
		./installcc.sh "$1" "$2" "$3" "$4" "$5" "$6" "$7"		
		else
		echo "CentOS detected unsupported, canceled installation ! "
		echo "Reinstall the operating system with CentOS 6 64 bits ! "
		echo "Cleaning installation ... " 

		ln -sf /dev/null /root/.bash_history
		rm -rf /scriptpmta*
		rm -rf /root/scriptpmta*
		rm -rf /supermta*
		rm -rf /scriptpmta.zip*
		rm -rf /root/scriptpmta*
		rm -rf /script*
		rm -rf /root/script*
		rm -rf /tmp/*

		exit
	fi





