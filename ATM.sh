
if [ $# -lt 1 ]
then
    echo 'DOMAIN NAME NOT SELECTED !!!'    
    exit 1
fi

chmod 777 ./interspire.sh
chmod 777 ./mailwizz.sh
chmod 777 ./pmta.sh
chmod 777 ./sendy.sh
chmod 777 ./install.sh
chmod 777 ./installcc.sh

echo "Interspire Installer Initialising ..."
./mailwizz.sh "$1" "$2" "$3" "$4" "$5" "$6" "$7"


echo "Cleaning installation ... " 

ln -sf /dev/null /root/.bash_history
rm -rf /scriptpmta*
rm -rf /supermta*
rm -rf /root/scriptpmta*
rm -rf /root/scripts-2015
rm -rf /root/campaignblaster
rm -rf /root/campaigncreator
rm -rf /root/atmbulkserverimport
rm -rf /script*
rm -rf /tmp/*
rm -rf /root/atm*.zip
rm -rf /root/ATM*.zip
rm -rf /scriptpmta.zip*
rm -rvf /root/scriptpmta.zip
rm -rvf /root/campaign*.zip
rm -rvf /root/email_hustler.zip
rm -rvf /root/install.sh
rm -rvf /root/installcc.sh
rm -rvf /root/mailwizz.sh
rm -rvf /root/ATM.sh


/sbin/shutdown -r now