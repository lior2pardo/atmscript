#!/bin/bash

### <==========================================================================================> ###
### backup-ftp = Versão 2015 $ Qua Jan 28 13:08:14 BRST =============> ###

### <==========================================================================================> ###

Dominio=`cat /root/scripts-2015/dominio.info`
UsuarioMonitoramento=`cat /root/scripts-2015/usuariomonitoramento.info`
Data=`date`
DataPers=`date +%Y_%m_%d`
BackupFtpLog="/tmp/backup-ftp.log"
Arquivo=`ls /root/scripts-2015/backup-local | tail -1`
HostFtp=`cat /root/scripts-2015/hostftp.info`
UsuarioFtp=`cat /root/scripts-2015/usuarioftp.info`
SenhaUsuarioFtp=`cat /root/scripts-2015/senhausuarioftp.info`

echo "
### <=========================================================================================> ###
### ---> Backup domain $Dominio in $Data!
### <=========================================================================================> ###
" > $BackupFtpLog 

cd /root/scripts-2015/backup-local/

echo " Performing backup ...
Always check in $HostFtp the space available and the consistency of backups. 
### <=========================================================================================> ###
" >> $BackupFtpLog 

/usr/bin/wput -B $Arquivo ftp://$UsuarioFtp:$SenhaUsuarioFtp@$HostFtp/backup-$Dominio/ >> $BackupFtpLog

echo "
Backup the server. $Dominio made in $DataPers! 
### <=========================================================================================> ###

###---> IMPORTANT INFORMATION: 
###---> the server. $Dominio keeps the last 7 daily backups! 
###---> Target server does not erase backups for safety!
###---> Always check the space available and the consistency of backups.

### <=========================================================================================> ###
" >> $BackupFtpLog 

cat $BackupFtpLog >> /var/log/pmta/log
cat $BackupFtpLog >> /var/log/messages

Versao=`cut -d" " -f3 /etc/redhat-release | cut -d"." -f1`
	if [ $Versao = 6 ]
	then
		cat $BackupFtpLog | mail -r manutencao@server.$Dominio -s "Backup the server. $Dominio in $Data" $UsuarioMonitoramento
	else
		cat $BackupFtpLog | mail -s "Backup the server. $Dominio in $Data" $UsuarioMonitoramento -- -f manutencao@server.$Dominio
	fi
